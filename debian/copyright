Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: piglit
Upstream-Contact: Piglit <piglit@lists.freedesktop.org>
Source: git://anongit.freedesktop.org/piglit

Files: *
Copyright: 2006 - 2014 Intel Corporation
           Intel 2011
           2007 - 2014 VMware, Inc.
           1999 - 2001 Allen Akin
           2012 Blaž Tomažič <blaz.tomazic@gmail.com>
           2009 - 2013 Red Hat, Inc.
           2010 - 2012 Red Hat
           2010 Fredrik Höglund (fredrik@kde.org)
           Christopher James Halse Rogers <christopher.halse.rogers at canonical.com>
           2004 - 2008 IBM Corporation
           2011 Marek Olšák
           2009 - 2013 Marek Olšák <maraeo@gmail.com>
           2010 - 2012 Mathias Fröhlich
           2013 - 2014 Advanced Micro Devices, Inc.
           2013 Chris Forbes
           2011 - 2013 LunarG, Inc.
           2007 - 2013 The Piglit project
           2011 - 2013 Henri Verbeet <hverbeet@gmail.com>
           2012 - 2013 Google Inc.
           2011 Christoph Bumiller
           2009 Nicolai Hähnle
           2014 Ilia Mirkin
           2013 Linaro
           2013 Linaro Inc
           2011 Vincent Lejeune
           2011 Dave Airlie
           2011 Dave Airlie <airlied@redhat.com>
           2013 Timothy Arceri <t_arceri@yahoo.com.au>
           2010 Kristóf Ralovich
           1999 - 2007 Brian Paul
           2013 Gregory Hainaut <gregory.hainaut@gmail.com>
           2013 Jan Vesely
           1994 Silicon Graphics, Inc.
           2014 Adel Gadllah <adel.gadllah@gmail.com>
           2010 Luca Barbieri
           2006 - 2008 Alexander Chemeris
           2010 Török Edwin
           2011 Pierre-Eric Pelloux-Prayer
           2011 - 2012 Con Kolivas
License: MIT

Files: debian/*
Copyright: 2014 Intel Corporation
License: MIT

Files: tests/general/triangle-guardband-viewport.c
Copyright: 2012 Google Inc.
License: LGPL-2.1+

Files: tests/shaders/glsl-uniform-out-of-bounds-2.c
Copyright: 2012 Google Inc.
License: LGPL-2.1+

Files: tests/util/glxew.h
Copyright: 2002-2008, Milan Ikits <milan ikits[]ieee org>
           2002-2008, Marcelo E. Magallon <mmagallo[]debian org>
           2002, Lev Povalahev
           1999-2007  Brian Paul   All Rights Reserved.
           2007 The Khronos Group Inc.
License: BSD-3-clause and MIT

Files: tests/glslparsertest/glsl2/gst-gl-*
Copyright: 2007 David A. Schleef <ds@schleef.org>
           2008 Filippo Argiolas <filippo.argiolas@gmail.com>
           2008-2009 Julien Isorce <julien.isorce@gmail.com>
           2008 Cyril Comparon <cyril.comparon@gmail.com>
License: LGPL-2+

Files: tests/cl/program/execute/gegl-fir-get-mean-component-1D-CL.cl
Copyright:  Copyright 2006 Dominik Ernst <dernst@gmx.de>
License: LGPL-3+

Files: tests/cl/program/execute/gegl-gamma-2-2-to-linear.cl
       tests/cl/program/execute/gegl-rgb-gamma-u8-to-ragabaf.cl
Copyright:  Copyright 2012 Victor Oliveira <victormatheus@gmail.com>
License: LGPL-3+

Files: tests/glslparsertest/glsl2/norsetto-*
Copyright: 2007 Cesare Tirabassi <norsetto@ubuntu.com>
License: GPL-3

Files: tests/cl/program/execute/pyrit-wpa-psk.cl
Copyright: Copyright 2008-2011 Lukas Lueg <lukas.lueg@gmail.com>
License: GPL-3+

Files: tests/glslparsertest/glsl2/xreal-*
Copyright: 2006 Robert Beckebans <trebor_7@users.sourceforge.net>
License: GPL-2+

Files: tests/glslparsertest/glsl2/xonotic-*
Copyright: 2006 Robert Beckebans <trebor_7@users.sourceforge.net>
License: GPL-2+

Files: tests/shaders/glsl-gnome-shell-dim-window.shader_test
Copyright: 2006 Robert Beckebans <trebor_7@users.sourceforge.net>
License: GPL-2+

Files: tests/glslparsertest/shaders/*
Copyright: 2002-2005  3Dlabs Inc. Ltd.
License: BSD-3-clause

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 BRIAN PAUL BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without 
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, 
   this list of conditions and the following disclaimer in the documentation 
   and/or other materials provided with the distribution.
 * The name of the author may be used to endorse or promote products 
   derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

License: LGPL-3+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-3'.

License: GPL-3
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
